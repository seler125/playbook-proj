import { makeAutoObservable } from 'mobx';
import { FormValues } from '../validation/formSchema';
import { roundNumber } from '../helpers/roundNumber';

export interface ExpensesItem {
    title: string;
    amount: number;
    eurAmount: number;
}
interface sumAmount {
    amount: number;
    eurAmount: number;
}

class ExpensesStore {
    items: ExpensesItem[] = [];
    conversionRate: number = 4.382;
    sum: sumAmount = { amount: 0, eurAmount: 0 };

    constructor() {
        makeAutoObservable(this);
    }

    addExpence({ amount, title }: FormValues) {
        console.log(amount, title);
        const item: ExpensesItem = {
            title,
            amount,
            eurAmount: roundNumber(amount / this.conversionRate),
        };

        this.items.push(item);
        this.sumItems();
    }

    deleteItem(index: number) {
        this.items.splice(index, 1);
        this.sumItems();
    }

    sumItems() {
        this.sum.amount = this.items.reduce((sum, item) => roundNumber(sum + item.amount), 0);
        this.sum.eurAmount = roundNumber(this.sum.amount / this.conversionRate);
    }

    changeConversionRate(newVal: number) {
        this.conversionRate = newVal;
        this.sumItems();
    }
}

const store = new ExpensesStore();
export default store;
