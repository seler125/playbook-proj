import '../styles/texts.css';
import '../styles/container.css';
import '../styles/button.css';
import { FC } from 'react';
import { observer } from 'mobx-react';
import store from '../store/ExpencesStore';
import { Formik, Form as FormikForm, Field, ErrorMessage } from 'formik';
import { conversionRateSchema, initialValues } from '../validation/conversionRateSchema';

export const Header: FC = observer(() => {
    return (
        <div className="header-container ">
            <div className="header-half-container">
                <h2 className="title">List of expences</h2>
            </div>

            <div className="header-half-container">
                <Formik
                    initialValues={initialValues}
                    validationSchema={conversionRateSchema}
                    onSubmit={(values) => {
                        store.changeConversionRate(values.conversionRate);
                    }}
                >
                    <FormikForm className="form-container">
                        <div className="input">
                            <div className="input-field">
                                <label htmlFor="conversionRate">1EUR =</label>
                                <Field type="number" id="conversionRate" name="conversionRate" />
                            </div>
                            <ErrorMessage className="error" name="conversionRate" component="div" />
                        </div>
                        <button type="submit" className="button">
                            Change conversion rate
                        </button>
                    </FormikForm>
                </Formik>
            </div>
        </div>
    );
});
