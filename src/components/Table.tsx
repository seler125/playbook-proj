import { FC } from 'react';
import { ExpensesItem } from '../store/ExpencesStore';
import { observer } from 'mobx-react';
import '../styles/table.css';
import '../styles/button.css';

export const Table: FC<{ items: ExpensesItem[]; deleteItem: (index: number) => void }> = observer(({ items, deleteItem }) => {
    return (
        <div className="table-container">
            <table>
                <thead>
                    <tr>
                        <th className="header-cell">Title</th>
                        <th className="header-cell">Amount(PLN)</th>
                        <th className="header-cell">Amount(EUR)</th>
                        <th className="header-cell">Options</th>
                    </tr>
                </thead>
                <tbody>
                    {items.map((item, index) => (
                        <tr key={`${item.title}- ${index}`}>
                            <th className="body-cell">{item.title}</th>
                            <th className="body-cell">{item.amount}</th>
                            <th className="body-cell">{item.eurAmount}</th>
                            <th className="body-cell">
                                <button onClick={() => deleteItem(index)} className="table-button">
                                    Delete{' '}
                                </button>
                            </th>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
});
