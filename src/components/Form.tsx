import { FC } from 'react';
import { Formik, Form as FormikForm, Field, ErrorMessage } from 'formik';
import { FormValues } from '../validation/formSchema';
import { initialValues, formSchema } from '../validation/formSchema';
import '../styles/container.css';
import '../styles/input.css';
import '../styles/button.css';

export const Form: FC<{ addExpense: (expense: FormValues) => void }> = ({ addExpense }) => {
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={formSchema}
            onSubmit={(values, { resetForm }) => {
                addExpense(values);
                resetForm();
            }}
        >
            <FormikForm className="form-container">
                <div className="input">
                    <div className="input-field">
                        <label htmlFor="title">Title:</label>
                        <Field type="text" id="title" name="title" />
                    </div>
                    <ErrorMessage name="title" component="div" className="error" />
                </div>
                <div className="input">
                    <div className="input-field">
                        <label htmlFor="amount">Amount:</label>
                        <Field type="number" id="amount" name="amount" />
                    </div>
                    <ErrorMessage name="amount" component="div" className="error" />
                </div>

                <button type="submit" className="button">
                    Submit
                </button>
            </FormikForm>
        </Formik>
    );
};
