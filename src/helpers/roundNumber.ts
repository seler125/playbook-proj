export const roundNumber = (num: number) => Math.round(num * 100) / 100;
