import { FC } from 'react';
import '../styles/container.css';
import '../styles/texts.css';
import { Form } from '../components/Form';
import store from '../store/ExpencesStore';
import { FormValues } from '../validation/formSchema';
import { Table } from '../components/Table';
import { observer } from 'mobx-react';
import { Header } from '../components/Header';

export const TableView: FC = observer(() => {
    const addExpense = (expense: FormValues) => {
        store.addExpence(expense);
    };

    const deleteItem = (index: number) => {
        store.deleteItem(index);
    };

    return (
        <div className="container">
            <div className="preview-box">
                <Header />
                <Form addExpense={addExpense} />
                <Table items={store.items} deleteItem={deleteItem} />
                <p className="basic-text">
                    {store.sum.amount}PLN ({store.sum.eurAmount})EUR
                </p>
            </div>
        </div>
    );
});
