import * as Yup from 'yup';

export interface FormValues {
    title: string;
    amount: number;
}

export const formSchema = Yup.object({
    title: Yup.string().min(5, 'Title must be at least 5 characters').required('Title is required'),
    amount: Yup.number()
        .min(0.1, 'Number must be bigger then 0')
        .test(
            'maxDigitsAfterDecimal',
            'There are allowed only two diggits after decimal point',
            (value) => !value || /^\d+(\.\d{1,2})?$/.test(value.toString()),
        )
        .required('Amount is required'),
});

export const initialValues = { title: '', amount: 0 };
