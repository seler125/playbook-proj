import * as Yup from 'yup';
import store from '../store/ExpencesStore';

export interface conversionSchema {
    conversionRate: number;
}

export const conversionRateSchema = Yup.object({
    conversionRate: Yup.number()
        .test(
            'maxDigitsAfterDecimal',
            'There are allowed only three diggits after decimal point',
            (value) => !value || /^\d+(\.\d{1,3})?$/.test(value.toString()),
        )
        .required('Amount is required'),
});

export const initialValues = { conversionRate: store.conversionRate };
